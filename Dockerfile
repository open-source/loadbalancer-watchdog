FROM alpine:latest

RUN apk add --update \
    php7 \
    php7-curl \
    php7-pcntl \
    php7-json \
    && rm -rf /var/cache/apk/*

WORKDIR /app

COPY . /app

CMD php -f WatchLB.php