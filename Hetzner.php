<?php

class Hetzner
{
    const API_URL = "https://api.hetzner.cloud";
    const LB_PREFIX = "LB";
    const SERVER_TYPE = "cpx21";
    const LOCATIONS = [
        1,
        2
    ];
    const SSH_KEY_ID = 1359164;

    const FLOATING_IPS = [
        325309,
        351659,
        351671
    ];


    public function __construct()
    {
    }

    /**
     * Starts a new Loadbalancer in Hetzner Cloud, moves the floating IPs to the new one and deletes the old server.
     * @returns TRUE on success, FALSE on failure or NULL if no internet connection
     */
    public function failoverServer()
    {
        $currentServer = $this->getLBServerID();
        if ($currentServer === null) {
            // Probably failed due to missing internet access
            return null;
        } else {
            $this->sendTelegramMessage("Starting Failover for " . $currentServer["name"]);
        }

        $snapshotID = $this->getSnapshotId();
        if ($snapshotID === null) {
            $this->sendTelegramMessage("Failed getting SnapshotID to use.");
            return false;
        }

        $currentLocationID = $this->getCurrentLocationId($currentServer["id"]);
        if ($currentLocationID === null) {
            $this->sendTelegramMessage("Failed getting LocationID to use.");
            return false;
        }
        $newLocationID = null;
        foreach (self::LOCATIONS as $location) {
            if ($location !== $currentLocationID) {
                $newLocationID = $location;
                break;
            }
        }

        $response = $this->postToHetznerApi("/v1/servers", [
            "name" => self::LB_PREFIX . "-" . time(),
            "server_type" => self::SERVER_TYPE,
            "start_after_create" => true,
            "image" => $snapshotID,
            "location" => $newLocationID,
            "ssh_keys" => [self::SSH_KEY_ID]
        ], 10);

        if ($response === null) {
            $this->sendTelegramMessage("Error creating new Server! Response null");
            return false;
        }
        $server = json_decode($response, true);
        if ($server === null) {
            $this->sendTelegramMessage("Error creating new Server! JSON decode failed for response: " . $response);
            return false;
        }

        $serverId = $server["server"]["id"];

        $timeoutServerUp = 600;
        $serverReady = $this->waitForServerStatus("running", $serverId, $timeoutServerUp);

        if (!$serverReady) {
            $this->sendTelegramMessage("New Server wasn't ready within $timeoutServerUp" . "s.");
            $this->deleteServer($serverId, 10);
            return false;
        }

        echo("Server Ready");

        // Once the new server is ready we can assign the floating IPs to it
        // The new server is up and running at this point
        // In case of network problems we will try assignment up to 30 times before giving up
        $success = true;
        $maxTries = 30;
        foreach (self::FLOATING_IPS as $floatingIP) {
            for ($counter = 1; $counter <= $maxTries; $counter ++) {
                $response = $this->postToHetznerApi("/v1/floating_ips/$floatingIP/actions/assign", [
                    "server" => $serverId
                    ], 10);
                if (!empty($response)) {
                    break;
                } else {
                    if ($counter === $maxTries) {
                        // The last try was a failure, too
                        $this->sendTelegramMessage("Couldn't assign floating IP to new server. Giving up! Please Check the server status manually");
                        $this->deleteServer($serverId);
                        return false;
                    }
                    sleep(1);
                }
            }
        }

        # Delete the old Server
        $response = $this->deleteServer($currentServer["id"], 10);
        if (empty($response)) {
            $this->sendTelegramMessage("Couldn't delete old server! Please manually check.");
        } else {
            $this->sendTelegramMessage("Successfully switched to new Loadbalancer.");
        }
        return true;
    }

    private function getCurrentLocationId($serverID)
    {
        $response = $this->fetchFromHetznerApi("/v1/servers/$serverID");
        if ($response === null) {
            return null;
        }
        $server = json_decode($response, true);
        if ($server === null) {
            return null;
        } else {
            $server = $server["server"];
        }
        return $server["datacenter"]["id"];
    }

    private function sendTelegramMessage($message)
    {
        $timeOutSeconds = 5;
        $url = "https://api.telegram.org/bot" . TELEGRAM_BOT_TOKEN . "/sendMessage";

        $params = [
            "chat_id" => TELEGRAM_CHAT_ID,
            "text" => $message
        ];

        $curlOpts = [
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_FAILONERROR => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => $timeOutSeconds,
            CURLOPT_LOW_SPEED_LIMIT => $timeOutSeconds * 10000,
            CURLOPT_LOW_SPEED_TIME => $timeOutSeconds,
            CURLOPT_TIMEOUT => $timeOutSeconds,
        ];

        $ch = curl_init();
        curl_setopt_array($ch, $curlOpts);
        $body = curl_exec($ch);
        try {
            if (curl_errno($ch)) {
                $errorMsg = curl_error($ch);
                if (!empty($errorMsg)) {
                    echo($errorMsg . PHP_EOL);
                }
                return null;
            } else {
                return $body;
            }
        } finally {
            curl_close($ch);
        }
    }

    public function getLBServerID()
    {
        $response = $this->fetchFromHetznerApi("/v1/servers");
        $servers = json_decode($response, true);
        if ($servers === null) {
            return null;
        }
        $lb = null;
        foreach ($servers["servers"] as $server) {
            if (stripos($server["name"], self::LB_PREFIX) === 0) {
                $lb = [
                "name" => $server["name"],
                "id" => $server["id"]
            ];
                break;
            }
        }
        return $lb;
    }

    public function deleteServer($serverId, $timeoutSeconds)
    {
        $result = $this->deleteFromHetznerApi("/v1/servers/$serverId", $timeoutSeconds);
        return $result;
    }

    private function waitForServerStatus($status, $serverId, $timeoutSeconds)
    {
        $startTime = microtime(true);
        while (microtime(true) - $startTime < $timeoutSeconds) {
            $response = $this->fetchFromHetznerApi("/v1/servers/$serverId");
            if ($response === null) {
                return false;
            }
            $server = json_decode($response, true);
            if ($server === null) {
                return false;
            }
            $server = $server["server"];
            if ($server["status"] === $status) {
                return true;
            } else {
                sleep(10);
            }
        }
    }


    private function fetchFromHetznerApi($apiRoute, $method = "GET")
    {
        $url = self::API_URL . $apiRoute;
        $headers = [
            "Authorization: Bearer " . API_KEY
        ];
    
        $curlOpts = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FAILONERROR => true,
            CURLOPT_HTTPHEADER => $headers
        ];
        $ch = curl_init();
        curl_setopt_array($ch, $curlOpts);
        try {
            $body = curl_exec($ch);
            if (curl_errno($ch)) {
                $errorMsg = curl_error($ch);
                if (!empty($errorMsg)) {
                    echo($errorMsg . PHP_EOL);
                }
                return null;
            } else {
                return $body;
            }
        } finally {
            curl_close($ch);
        }
    }

    private function getSnapshotId()
    {
        $response = $this->fetchFromHetznerApi("/v1/images?type=snapshot");
        if ($response === null) {
            return null;
        }
        $snapshots = json_decode($response, true);
        if ($snapshots === null) {
            return null;
        }
        // There might be multiple Snapshots. We will use the Prefix "LB" and use the most current one
        $snapshotToUse = null;
        $snapshotToUseDate = null;
        foreach ($snapshots["images"] as $snapshot) {
            if (stripos($snapshot["description"], self::LB_PREFIX) !== 0) {
                continue;
            }
            $date = DateTime::createFromFormat("Y-m-d\TH:i:sT", $snapshot["created"]);
            if ($snapshotToUseDate === null || $date > $snapshotToUseDate) {
                $snapshotToUse = $snapshot["id"];
                $snapshotToUseDate = $date;
            }
        }
        return $snapshotToUse;
    }

    private function postToHetznerApi($apiRoute, $jsonPostData, $timeOutSeconds)
    {
        $url = self::API_URL . $apiRoute;
        $headers = [
        "Authorization: Bearer " . API_KEY,
        "Content-Type: application/json"
        ];

        $curlOpts = [
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FAILONERROR => true,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_CONNECTTIMEOUT => $timeOutSeconds,
            CURLOPT_LOW_SPEED_LIMIT => $timeOutSeconds * 10000,
            CURLOPT_LOW_SPEED_TIME => $timeOutSeconds,
            CURLOPT_TIMEOUT => $timeOutSeconds,
        ];
        if (!empty($jsonPostData)) {
            $curlOpts[CURLOPT_POSTFIELDS] = json_encode($jsonPostData);
        }
        $ch = curl_init();
        curl_setopt_array($ch, $curlOpts);
        $body = curl_exec($ch);
        try {
            if (curl_errno($ch)) {
                $errorMsg = curl_error($ch);
                if (!empty($errorMsg)) {
                    echo($errorMsg . PHP_EOL);
                }
                return null;
            } else {
                return $body;
            }
        } finally {
            curl_close($ch);
        }
    }

    private function deleteFromHetznerApi($apiRoute, $timeOutSeconds)
    {
        $url = self::API_URL . $apiRoute;
        $headers = [
            "Authorization: Bearer " . API_KEY,
            "Content-Type: application/json"
        ];

        $curlOpts = [
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FAILONERROR => true,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_CONNECTTIMEOUT => $timeOutSeconds,
            CURLOPT_LOW_SPEED_LIMIT => $timeOutSeconds * 10000,
            CURLOPT_LOW_SPEED_TIME => $timeOutSeconds,
            CURLOPT_TIMEOUT => $timeOutSeconds,
        ];

        $ch = curl_init();
        curl_setopt_array($ch, $curlOpts);
        try {
            $body = curl_exec($ch);
            if (curl_errno($ch)) {
                $errorMsg = curl_error($ch);
                if (!empty($errorMsg)) {
                    echo($errorMsg . PHP_EOL);
                }
                return null;
            } else {
                return $body;
            }
        } finally {
            curl_close($ch);
        }
    }
}
