<?php

$url = "https://metager.de";
    
$ch = curl_init();
$curlOpts = [
    CURLOPT_URL => $url,
    CURLOPT_FAILONERROR => true,
    CURLOPT_NOBODY => true,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_RESOLVE => ["metager.de:443:49.12.116.58"],
    CURLOPT_CONNECTTIMEOUT => 45,
    CURLOPT_LOW_SPEED_LIMIT => 45 * 10000,
    CURLOPT_LOW_SPEED_TIME => 45,
    CURLOPT_TIMEOUT => 45,
];
curl_setopt_array($ch, $curlOpts);

$failedAttempts = 0;
$downSince = null;
try {
    while (true) {
        $body = curl_exec($ch);
        if (curl_errno($ch)) {
            $errorMsg = curl_error($ch);
            if (!empty($errorMsg)) {
                echo($errorMsg . PHP_EOL);
            }
            if ($failedAttempts === 0) {
                echo("[" . date("H:i:s") . "] Host metager.de is down" . PHP_EOL);
                $downSince = microtime(true);
            }
            $failedAttempts++;
        } else {
            if ($failedAttempts > 0) {
                $failedAttempts = 0;
                echo("[" . date("H:i:s") . "] Host metager.de is up again. Was down for " . round((microtime(true)-$downSince)) . "s" . PHP_EOL);
                $downSince = null;
            }
        }
        sleep(1);
    }
} finally {
    echo("Exitting");
    curL_close($ch);
}
