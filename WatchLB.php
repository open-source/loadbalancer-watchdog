<?php

declare(ticks = 1);
pcntl_signal(SIGINT, 'pidfile_close');
pcntl_signal(SIGTERM, 'pidfile_close');
register_shutdown_function('pidfile_close');

include "Hetzner.php";

define("PIDFILE", "/tmp/WatchLB.pid");
define("HEALTH_CHECK_URL", getenv("HEALTH_CHECK_URL"));
define("TELEGRAM_BOT_TOKEN", getenv("TELEGRAM_BOT_TOKEN"));
define("TELEGRAM_CHAT_ID", getenv("TELEGRAM_CHAT_ID"));
define("API_KEY", getenv("API_KEY"));
pidfile_open();


/**
 * Checking health status in a loop
 */
$shouldRun = true;
$firstfailedCheck = null;
$lastFailover = null;
// Interval in seconds for the healthcheck. For both failing and healthy states
// Failover interval defines the number of seconds to wait for healthy state before
// deploying a new server. Should prevent short outpage from triggering failover
// Failover procedure cannot be started more often than failover-every seconds
$interval = [
    "healthy" => 15,
    "failing" => 5,
    "failover" => 30,
    "failover-every" => 10 * 60
];


while ($shouldRun) {
    $checkStarted = microtime(true);
    $response = executeHealthCheck();
    if ($response) {
        // Healthcheck succeeded
        if ($firstfailedCheck !== null) {
            printOut("Status became healthy again");
            $firstfailedCheck = null;
        }
    } else {
        if ($firstfailedCheck === null) {
            printOut("Status became unhealthy");
            $firstfailedCheck = microtime(true);
        }
    }

    // Start failover procedure if status is unhealthy for long enough and we haven't done it recently
    if ($firstfailedCheck !== null &&
        microtime(true) - $firstfailedCheck >= $interval["failover"] &&
        ($lastFailover === null || microtime(true) - $lastFailover >= $interval["failover-every"])) {
        printOut("Starting failover");
        $hetzner = new Hetzner();
        $result = $hetzner->failoverServer();

        if ($result === null) {
            // The problem seems to be the internet connection of this server. Resetting
            printOut("Cannot run failover. Please check the Internet connection. Trying again in a minute");
            $lastFailover = microtime(true) - ($interval["failover-every"] - 60);
        } else {
            $lastFailover = microtime(true);
        }
    }

    $sleepTime = $firstfailedCheck === null ? $interval["healthy"] : $interval["failing"];
    $sleepTime = max(round(($sleepTime - (microtime(true)-$checkStarted)) * 1000000), 0);
    usleep($sleepTime);
}


function printOut($message)
{
    echo("[" . date("H:i:s") . "] $message" . PHP_EOL);
}

function executeHealthCheck()
{
    $ch = curl_init();
    $curlOpts = [
        CURLOPT_URL => HEALTH_CHECK_URL,
        CURLOPT_FAILONERROR => true,
        CURLOPT_HEADER => true,
        CURLOPT_NOBODY => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CONNECTTIMEOUT => 2,
        CURLOPT_LOW_SPEED_LIMIT => 20000,
        CURLOPT_LOW_SPEED_TIME => 2,
        CURLOPT_TIMEOUT => 2,
    ];
    curl_setopt_array($ch, $curlOpts);
    
    $body = curl_exec($ch);
    if (curl_errno($ch)) {
        $errorMsg = curl_error($ch);
        if (!empty($errorMsg)) {
            echo($errorMsg . PHP_EOL);
        }
        return false;
    } else {
        return true;
    }
}

function pidfile_open()
{
    if (file_exists(PIDFILE)) {
        exit;
    }
    $handle = fopen(PIDFILE, 'x');
    if (!$handle) {
        exit;
    }
    $pid = getmypid();
    fwrite($handle, $pid);
    fclose($handle);

    if (empty(HEALTH_CHECK_URL)) {
        echo "[env] required environment variable \"HEALTH_CHECK_URL\" not set." . PHP_EOL;
        exit;
    }
    if (empty(TELEGRAM_BOT_TOKEN)) {
        echo "[env] required environment variable \"TELEGRAM_BOT_TOKEN\" not set." . PHP_EOL;
        exit;
    }
    if (empty(TELEGRAM_CHAT_ID)) {
        echo "[env] required environment variable \"TELEGRAM_CHAT_IDFILE\" not set." . PHP_EOL;
        exit;
    }
    if (empty(API_KEY)) {
        echo "[env] required environment variable \"API_KEY\" not set." . PHP_EOL;
        exit;
    }
}

function pidfile_close($sig = null)
{
    if (file_exists(PIDFILE)) {
        unlink(PIDFILE);
    }
    if ($sig !== null) {
        echo(PHP_EOL . "Bye" . PHP_EOL);
        exit;
    }
}
